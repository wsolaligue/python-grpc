import grpc

import connect
from lucidworks.connector.connector_service_pb2_grpc import ConnectorApiServiceStub

if __name__ == "__main__":
    # open a gRPC channel
    channel = grpc.insecure_channel('127.0.0.1:8771')

    stub = ConnectorApiServiceStub(channel)
    connect = connect.Connect(stub)
    connect.execute()
