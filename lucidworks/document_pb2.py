# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: lucidworks/document.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='lucidworks/document.proto',
  package='lucidworks.document',
  syntax='proto3',
  serialized_pb=_b('\n\x19lucidworks/document.proto\x12\x13lucidworks.document\"\xae\x03\n\x11\x44\x61taStreamRequest\x12J\n\x08metadata\x18\x01 \x01(\x0b\x32\x36.lucidworks.document.DataStreamRequest.MetadataPayloadH\x00\x12\x43\n\x04\x64\x61ta\x18\x02 \x01(\x0b\x32\x33.lucidworks.document.DataStreamRequest.ChunkPayloadH\x00\x1a\xac\x01\n\x0fMetadataPayload\x12\x10\n\x08streamId\x18\x01 \x01(\r\x12V\n\x08metadata\x18\x02 \x03(\x0b\x32\x44.lucidworks.document.DataStreamRequest.MetadataPayload.MetadataEntry\x1a/\n\rMetadataEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t:\x02\x38\x01\x1aN\n\x0c\x43hunkPayload\x12\x10\n\x08streamId\x18\x01 \x01(\r\x12\x0e\n\x06length\x18\x02 \x01(\r\x12\r\n\x05\x63hunk\x18\x03 \x01(\x0c\x12\r\n\x05\x63rc32\x18\x04 \x01(\x05\x42\t\n\x07payload\"f\n\x0f\x44\x61taStreamReply\x12\x10\n\x08streamId\x18\x01 \x01(\t\x12\x0f\n\x07message\x18\x02 \x01(\t\x12\x30\n\tdocuments\x18\x03 \x03(\x0b\x32\x1d.lucidworks.document.Document\"\xa7\x06\n\x08\x44ocument\x12\r\n\x05\x64ocId\x18\x01 \x01(\t\x12\x0f\n\x07\x63omment\x18\x02 \x01(\t\x12=\n\x08metadata\x18\x03 \x03(\x0b\x32+.lucidworks.document.Document.MetadataEntry\x12\x39\n\x06\x66ields\x18\x04 \x03(\x0b\x32).lucidworks.document.Document.FieldsEntry\x1aQ\n\rMetadataEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12/\n\x05value\x18\x02 \x01(\x0b\x32 .lucidworks.document.ScalarValue:\x02\x38\x01\x1aR\n\x0b\x46ieldsEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\x32\n\x05value\x18\x02 \x01(\x0b\x32#.lucidworks.document.Document.Field:\x02\x38\x01\x1a\xd9\x03\n\x05\x46ield\x12\x0b\n\x03key\x18\x01 \x01(\t\x12/\n\x05value\x18\x02 \x01(\x0b\x32 .lucidworks.document.ScalarValue\x12\x37\n\x05hints\x18\x03 \x03(\x0e\x32(.lucidworks.document.Document.Field.Hint\x12\x43\n\x0b\x61nnotations\x18\x04 \x03(\x0b\x32..lucidworks.document.Document.Field.Annotation\x12\x43\n\x08metadata\x18\x05 \x03(\x0b\x32\x31.lucidworks.document.Document.Field.MetadataEntry\x1a\x36\n\nAnnotation\x12\r\n\x05start\x18\x01 \x01(\r\x12\x0b\n\x03\x65nd\x18\x02 \x01(\r\x12\x0c\n\x04type\x18\x03 \x01(\t\x1a/\n\rMetadataEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t:\x02\x38\x01\"f\n\x04Hint\x12\x16\n\x12\x46IELD_MULTI_VALUED\x10\x00\x12\x17\n\x13\x46IELD_SINGLE_VALUED\x10\x01\x12\r\n\tTYPE_TEXT\x10\x02\x12\x0f\n\x0bTYPE_STRING\x10\x03\x12\r\n\tTYPE_DATE\x10\x04\"\xb4\x01\n\x0bScalarValue\x12\x16\n\x0c\x64ouble_value\x18\x01 \x01(\x01H\x00\x12\x15\n\x0b\x66loat_value\x18\x02 \x01(\x02H\x00\x12\x13\n\tint_value\x18\x03 \x01(\x05H\x00\x12\x14\n\nlong_value\x18\x04 \x01(\x03H\x00\x12\x14\n\nbool_value\x18\x05 \x01(\x08H\x00\x12\x16\n\x0cstring_value\x18\x06 \x01(\tH\x00\x12\x14\n\nbyte_value\x18\x07 \x01(\x0cH\x00\x42\x07\n\x05value2h\n\x08Uploader\x12\\\n\x08\x44oUpload\x12&.lucidworks.document.DataStreamRequest\x1a$.lucidworks.document.DataStreamReply\"\x00(\x01\x42\x37\n$com.lucidworks.fusion.proto.documentB\rDocumentProtoP\x01\x62\x06proto3')
)



_DOCUMENT_FIELD_HINT = _descriptor.EnumDescriptor(
  name='Hint',
  full_name='lucidworks.document.Document.Field.Hint',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='FIELD_MULTI_VALUED', index=0, number=0,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FIELD_SINGLE_VALUED', index=1, number=1,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TYPE_TEXT', index=2, number=2,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TYPE_STRING', index=3, number=3,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TYPE_DATE', index=4, number=4,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=1293,
  serialized_end=1395,
)
_sym_db.RegisterEnumDescriptor(_DOCUMENT_FIELD_HINT)


_DATASTREAMREQUEST_METADATAPAYLOAD_METADATAENTRY = _descriptor.Descriptor(
  name='MetadataEntry',
  full_name='lucidworks.document.DataStreamRequest.MetadataPayload.MetadataEntry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='lucidworks.document.DataStreamRequest.MetadataPayload.MetadataEntry.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='lucidworks.document.DataStreamRequest.MetadataPayload.MetadataEntry.value', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=_descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001')),
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=343,
  serialized_end=390,
)

_DATASTREAMREQUEST_METADATAPAYLOAD = _descriptor.Descriptor(
  name='MetadataPayload',
  full_name='lucidworks.document.DataStreamRequest.MetadataPayload',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='streamId', full_name='lucidworks.document.DataStreamRequest.MetadataPayload.streamId', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='metadata', full_name='lucidworks.document.DataStreamRequest.MetadataPayload.metadata', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_DATASTREAMREQUEST_METADATAPAYLOAD_METADATAENTRY, ],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=218,
  serialized_end=390,
)

_DATASTREAMREQUEST_CHUNKPAYLOAD = _descriptor.Descriptor(
  name='ChunkPayload',
  full_name='lucidworks.document.DataStreamRequest.ChunkPayload',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='streamId', full_name='lucidworks.document.DataStreamRequest.ChunkPayload.streamId', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='length', full_name='lucidworks.document.DataStreamRequest.ChunkPayload.length', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='chunk', full_name='lucidworks.document.DataStreamRequest.ChunkPayload.chunk', index=2,
      number=3, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='crc32', full_name='lucidworks.document.DataStreamRequest.ChunkPayload.crc32', index=3,
      number=4, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=392,
  serialized_end=470,
)

_DATASTREAMREQUEST = _descriptor.Descriptor(
  name='DataStreamRequest',
  full_name='lucidworks.document.DataStreamRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='metadata', full_name='lucidworks.document.DataStreamRequest.metadata', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='data', full_name='lucidworks.document.DataStreamRequest.data', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_DATASTREAMREQUEST_METADATAPAYLOAD, _DATASTREAMREQUEST_CHUNKPAYLOAD, ],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='payload', full_name='lucidworks.document.DataStreamRequest.payload',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=51,
  serialized_end=481,
)


_DATASTREAMREPLY = _descriptor.Descriptor(
  name='DataStreamReply',
  full_name='lucidworks.document.DataStreamReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='streamId', full_name='lucidworks.document.DataStreamReply.streamId', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='message', full_name='lucidworks.document.DataStreamReply.message', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='documents', full_name='lucidworks.document.DataStreamReply.documents', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=483,
  serialized_end=585,
)


_DOCUMENT_METADATAENTRY = _descriptor.Descriptor(
  name='MetadataEntry',
  full_name='lucidworks.document.Document.MetadataEntry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='lucidworks.document.Document.MetadataEntry.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='lucidworks.document.Document.MetadataEntry.value', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=_descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001')),
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=754,
  serialized_end=835,
)

_DOCUMENT_FIELDSENTRY = _descriptor.Descriptor(
  name='FieldsEntry',
  full_name='lucidworks.document.Document.FieldsEntry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='lucidworks.document.Document.FieldsEntry.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='lucidworks.document.Document.FieldsEntry.value', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=_descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001')),
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=837,
  serialized_end=919,
)

_DOCUMENT_FIELD_ANNOTATION = _descriptor.Descriptor(
  name='Annotation',
  full_name='lucidworks.document.Document.Field.Annotation',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='start', full_name='lucidworks.document.Document.Field.Annotation.start', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='end', full_name='lucidworks.document.Document.Field.Annotation.end', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='type', full_name='lucidworks.document.Document.Field.Annotation.type', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1188,
  serialized_end=1242,
)

_DOCUMENT_FIELD_METADATAENTRY = _descriptor.Descriptor(
  name='MetadataEntry',
  full_name='lucidworks.document.Document.Field.MetadataEntry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='lucidworks.document.Document.Field.MetadataEntry.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='lucidworks.document.Document.Field.MetadataEntry.value', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=_descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001')),
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=343,
  serialized_end=390,
)

_DOCUMENT_FIELD = _descriptor.Descriptor(
  name='Field',
  full_name='lucidworks.document.Document.Field',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='lucidworks.document.Document.Field.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='lucidworks.document.Document.Field.value', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='hints', full_name='lucidworks.document.Document.Field.hints', index=2,
      number=3, type=14, cpp_type=8, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='annotations', full_name='lucidworks.document.Document.Field.annotations', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='metadata', full_name='lucidworks.document.Document.Field.metadata', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_DOCUMENT_FIELD_ANNOTATION, _DOCUMENT_FIELD_METADATAENTRY, ],
  enum_types=[
    _DOCUMENT_FIELD_HINT,
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=922,
  serialized_end=1395,
)

_DOCUMENT = _descriptor.Descriptor(
  name='Document',
  full_name='lucidworks.document.Document',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='docId', full_name='lucidworks.document.Document.docId', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='comment', full_name='lucidworks.document.Document.comment', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='metadata', full_name='lucidworks.document.Document.metadata', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='fields', full_name='lucidworks.document.Document.fields', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_DOCUMENT_METADATAENTRY, _DOCUMENT_FIELDSENTRY, _DOCUMENT_FIELD, ],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=588,
  serialized_end=1395,
)


_SCALARVALUE = _descriptor.Descriptor(
  name='ScalarValue',
  full_name='lucidworks.document.ScalarValue',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='double_value', full_name='lucidworks.document.ScalarValue.double_value', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='float_value', full_name='lucidworks.document.ScalarValue.float_value', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='int_value', full_name='lucidworks.document.ScalarValue.int_value', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='long_value', full_name='lucidworks.document.ScalarValue.long_value', index=3,
      number=4, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='bool_value', full_name='lucidworks.document.ScalarValue.bool_value', index=4,
      number=5, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='string_value', full_name='lucidworks.document.ScalarValue.string_value', index=5,
      number=6, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='byte_value', full_name='lucidworks.document.ScalarValue.byte_value', index=6,
      number=7, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='value', full_name='lucidworks.document.ScalarValue.value',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=1398,
  serialized_end=1578,
)

_DATASTREAMREQUEST_METADATAPAYLOAD_METADATAENTRY.containing_type = _DATASTREAMREQUEST_METADATAPAYLOAD
_DATASTREAMREQUEST_METADATAPAYLOAD.fields_by_name['metadata'].message_type = _DATASTREAMREQUEST_METADATAPAYLOAD_METADATAENTRY
_DATASTREAMREQUEST_METADATAPAYLOAD.containing_type = _DATASTREAMREQUEST
_DATASTREAMREQUEST_CHUNKPAYLOAD.containing_type = _DATASTREAMREQUEST
_DATASTREAMREQUEST.fields_by_name['metadata'].message_type = _DATASTREAMREQUEST_METADATAPAYLOAD
_DATASTREAMREQUEST.fields_by_name['data'].message_type = _DATASTREAMREQUEST_CHUNKPAYLOAD
_DATASTREAMREQUEST.oneofs_by_name['payload'].fields.append(
  _DATASTREAMREQUEST.fields_by_name['metadata'])
_DATASTREAMREQUEST.fields_by_name['metadata'].containing_oneof = _DATASTREAMREQUEST.oneofs_by_name['payload']
_DATASTREAMREQUEST.oneofs_by_name['payload'].fields.append(
  _DATASTREAMREQUEST.fields_by_name['data'])
_DATASTREAMREQUEST.fields_by_name['data'].containing_oneof = _DATASTREAMREQUEST.oneofs_by_name['payload']
_DATASTREAMREPLY.fields_by_name['documents'].message_type = _DOCUMENT
_DOCUMENT_METADATAENTRY.fields_by_name['value'].message_type = _SCALARVALUE
_DOCUMENT_METADATAENTRY.containing_type = _DOCUMENT
_DOCUMENT_FIELDSENTRY.fields_by_name['value'].message_type = _DOCUMENT_FIELD
_DOCUMENT_FIELDSENTRY.containing_type = _DOCUMENT
_DOCUMENT_FIELD_ANNOTATION.containing_type = _DOCUMENT_FIELD
_DOCUMENT_FIELD_METADATAENTRY.containing_type = _DOCUMENT_FIELD
_DOCUMENT_FIELD.fields_by_name['value'].message_type = _SCALARVALUE
_DOCUMENT_FIELD.fields_by_name['hints'].enum_type = _DOCUMENT_FIELD_HINT
_DOCUMENT_FIELD.fields_by_name['annotations'].message_type = _DOCUMENT_FIELD_ANNOTATION
_DOCUMENT_FIELD.fields_by_name['metadata'].message_type = _DOCUMENT_FIELD_METADATAENTRY
_DOCUMENT_FIELD.containing_type = _DOCUMENT
_DOCUMENT_FIELD_HINT.containing_type = _DOCUMENT_FIELD
_DOCUMENT.fields_by_name['metadata'].message_type = _DOCUMENT_METADATAENTRY
_DOCUMENT.fields_by_name['fields'].message_type = _DOCUMENT_FIELDSENTRY
_SCALARVALUE.oneofs_by_name['value'].fields.append(
  _SCALARVALUE.fields_by_name['double_value'])
_SCALARVALUE.fields_by_name['double_value'].containing_oneof = _SCALARVALUE.oneofs_by_name['value']
_SCALARVALUE.oneofs_by_name['value'].fields.append(
  _SCALARVALUE.fields_by_name['float_value'])
_SCALARVALUE.fields_by_name['float_value'].containing_oneof = _SCALARVALUE.oneofs_by_name['value']
_SCALARVALUE.oneofs_by_name['value'].fields.append(
  _SCALARVALUE.fields_by_name['int_value'])
_SCALARVALUE.fields_by_name['int_value'].containing_oneof = _SCALARVALUE.oneofs_by_name['value']
_SCALARVALUE.oneofs_by_name['value'].fields.append(
  _SCALARVALUE.fields_by_name['long_value'])
_SCALARVALUE.fields_by_name['long_value'].containing_oneof = _SCALARVALUE.oneofs_by_name['value']
_SCALARVALUE.oneofs_by_name['value'].fields.append(
  _SCALARVALUE.fields_by_name['bool_value'])
_SCALARVALUE.fields_by_name['bool_value'].containing_oneof = _SCALARVALUE.oneofs_by_name['value']
_SCALARVALUE.oneofs_by_name['value'].fields.append(
  _SCALARVALUE.fields_by_name['string_value'])
_SCALARVALUE.fields_by_name['string_value'].containing_oneof = _SCALARVALUE.oneofs_by_name['value']
_SCALARVALUE.oneofs_by_name['value'].fields.append(
  _SCALARVALUE.fields_by_name['byte_value'])
_SCALARVALUE.fields_by_name['byte_value'].containing_oneof = _SCALARVALUE.oneofs_by_name['value']
DESCRIPTOR.message_types_by_name['DataStreamRequest'] = _DATASTREAMREQUEST
DESCRIPTOR.message_types_by_name['DataStreamReply'] = _DATASTREAMREPLY
DESCRIPTOR.message_types_by_name['Document'] = _DOCUMENT
DESCRIPTOR.message_types_by_name['ScalarValue'] = _SCALARVALUE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

DataStreamRequest = _reflection.GeneratedProtocolMessageType('DataStreamRequest', (_message.Message,), dict(

  MetadataPayload = _reflection.GeneratedProtocolMessageType('MetadataPayload', (_message.Message,), dict(

    MetadataEntry = _reflection.GeneratedProtocolMessageType('MetadataEntry', (_message.Message,), dict(
      DESCRIPTOR = _DATASTREAMREQUEST_METADATAPAYLOAD_METADATAENTRY,
      __module__ = 'lucidworks.document_pb2'
      # @@protoc_insertion_point(class_scope:lucidworks.document.DataStreamRequest.MetadataPayload.MetadataEntry)
      ))
    ,
    DESCRIPTOR = _DATASTREAMREQUEST_METADATAPAYLOAD,
    __module__ = 'lucidworks.document_pb2'
    # @@protoc_insertion_point(class_scope:lucidworks.document.DataStreamRequest.MetadataPayload)
    ))
  ,

  ChunkPayload = _reflection.GeneratedProtocolMessageType('ChunkPayload', (_message.Message,), dict(
    DESCRIPTOR = _DATASTREAMREQUEST_CHUNKPAYLOAD,
    __module__ = 'lucidworks.document_pb2'
    # @@protoc_insertion_point(class_scope:lucidworks.document.DataStreamRequest.ChunkPayload)
    ))
  ,
  DESCRIPTOR = _DATASTREAMREQUEST,
  __module__ = 'lucidworks.document_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.document.DataStreamRequest)
  ))
_sym_db.RegisterMessage(DataStreamRequest)
_sym_db.RegisterMessage(DataStreamRequest.MetadataPayload)
_sym_db.RegisterMessage(DataStreamRequest.MetadataPayload.MetadataEntry)
_sym_db.RegisterMessage(DataStreamRequest.ChunkPayload)

DataStreamReply = _reflection.GeneratedProtocolMessageType('DataStreamReply', (_message.Message,), dict(
  DESCRIPTOR = _DATASTREAMREPLY,
  __module__ = 'lucidworks.document_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.document.DataStreamReply)
  ))
_sym_db.RegisterMessage(DataStreamReply)

Document = _reflection.GeneratedProtocolMessageType('Document', (_message.Message,), dict(

  MetadataEntry = _reflection.GeneratedProtocolMessageType('MetadataEntry', (_message.Message,), dict(
    DESCRIPTOR = _DOCUMENT_METADATAENTRY,
    __module__ = 'lucidworks.document_pb2'
    # @@protoc_insertion_point(class_scope:lucidworks.document.Document.MetadataEntry)
    ))
  ,

  FieldsEntry = _reflection.GeneratedProtocolMessageType('FieldsEntry', (_message.Message,), dict(
    DESCRIPTOR = _DOCUMENT_FIELDSENTRY,
    __module__ = 'lucidworks.document_pb2'
    # @@protoc_insertion_point(class_scope:lucidworks.document.Document.FieldsEntry)
    ))
  ,

  Field = _reflection.GeneratedProtocolMessageType('Field', (_message.Message,), dict(

    Annotation = _reflection.GeneratedProtocolMessageType('Annotation', (_message.Message,), dict(
      DESCRIPTOR = _DOCUMENT_FIELD_ANNOTATION,
      __module__ = 'lucidworks.document_pb2'
      # @@protoc_insertion_point(class_scope:lucidworks.document.Document.Field.Annotation)
      ))
    ,

    MetadataEntry = _reflection.GeneratedProtocolMessageType('MetadataEntry', (_message.Message,), dict(
      DESCRIPTOR = _DOCUMENT_FIELD_METADATAENTRY,
      __module__ = 'lucidworks.document_pb2'
      # @@protoc_insertion_point(class_scope:lucidworks.document.Document.Field.MetadataEntry)
      ))
    ,
    DESCRIPTOR = _DOCUMENT_FIELD,
    __module__ = 'lucidworks.document_pb2'
    # @@protoc_insertion_point(class_scope:lucidworks.document.Document.Field)
    ))
  ,
  DESCRIPTOR = _DOCUMENT,
  __module__ = 'lucidworks.document_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.document.Document)
  ))
_sym_db.RegisterMessage(Document)
_sym_db.RegisterMessage(Document.MetadataEntry)
_sym_db.RegisterMessage(Document.FieldsEntry)
_sym_db.RegisterMessage(Document.Field)
_sym_db.RegisterMessage(Document.Field.Annotation)
_sym_db.RegisterMessage(Document.Field.MetadataEntry)

ScalarValue = _reflection.GeneratedProtocolMessageType('ScalarValue', (_message.Message,), dict(
  DESCRIPTOR = _SCALARVALUE,
  __module__ = 'lucidworks.document_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.document.ScalarValue)
  ))
_sym_db.RegisterMessage(ScalarValue)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n$com.lucidworks.fusion.proto.documentB\rDocumentProtoP\001'))
_DATASTREAMREQUEST_METADATAPAYLOAD_METADATAENTRY.has_options = True
_DATASTREAMREQUEST_METADATAPAYLOAD_METADATAENTRY._options = _descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001'))
_DOCUMENT_METADATAENTRY.has_options = True
_DOCUMENT_METADATAENTRY._options = _descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001'))
_DOCUMENT_FIELDSENTRY.has_options = True
_DOCUMENT_FIELDSENTRY._options = _descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001'))
_DOCUMENT_FIELD_METADATAENTRY.has_options = True
_DOCUMENT_FIELD_METADATAENTRY._options = _descriptor._ParseOptions(descriptor_pb2.MessageOptions(), _b('8\001'))

_UPLOADER = _descriptor.ServiceDescriptor(
  name='Uploader',
  full_name='lucidworks.document.Uploader',
  file=DESCRIPTOR,
  index=0,
  options=None,
  serialized_start=1580,
  serialized_end=1684,
  methods=[
  _descriptor.MethodDescriptor(
    name='DoUpload',
    full_name='lucidworks.document.Uploader.DoUpload',
    index=0,
    containing_service=None,
    input_type=_DATASTREAMREQUEST,
    output_type=_DATASTREAMREPLY,
    options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_UPLOADER)

DESCRIPTOR.services_by_name['Uploader'] = _UPLOADER

# @@protoc_insertion_point(module_scope)
