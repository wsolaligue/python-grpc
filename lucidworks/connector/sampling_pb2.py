# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: lucidworks/connector/sampling.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from lucidworks import property_map_pb2 as lucidworks_dot_property__map__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='lucidworks/connector/sampling.proto',
  package='lucidworks.connector.sampling',
  syntax='proto3',
  serialized_pb=_b('\n#lucidworks/connector/sampling.proto\x12\x1dlucidworks.connector.sampling\x1a\x1dlucidworks/property-map.proto\"}\n\x03Req\x12\x10\n\x08\x63onfigId\x18\x01 \x01(\t\x12\r\n\x05limit\x18\x02 \x01(\x05\x12\x0f\n\x07waitSec\x18\x03 \x01(\x05\x12\x0f\n\x07refresh\x18\x04 \x01(\x08\x12\x16\n\x0emaxFieldLength\x18\x05 \x01(\x05\x12\x1b\n\x13\x65xcludeBinaryFields\x18\x06 \x01(\x08\"g\n\x08\x44\x65\x62ugReq\x12\x35\n\tsampleReq\x18\x01 \x01(\x0b\x32\".lucidworks.connector.sampling.Req\x12\x12\n\npipelineId\x18\x02 \x01(\t\x12\x10\n\x08parserId\x18\x03 \x01(\t\"=\n\x03Res\x12\x36\n\x11pipelineDocuments\x18\x01 \x03(\x0b\x32\x1b.lucidworks.propertymap.Map\"\x8a\x01\n\x08\x44\x65\x62ugRes\x12\x31\n\x0cstageConfigs\x18\x01 \x03(\x0b\x32\x1b.lucidworks.propertymap.Map\x12+\n\x06output\x18\x02 \x01(\x0b\x32\x1b.lucidworks.propertymap.Map\x12\x1e\n\x16outputEncounteredCount\x18\x03 \x01(\x03\x42\x42\n.com.lucidworks.fusion.proto.connector.samplingB\x0eSamplingProtosP\x00\x62\x06proto3')
  ,
  dependencies=[lucidworks_dot_property__map__pb2.DESCRIPTOR,])




_REQ = _descriptor.Descriptor(
  name='Req',
  full_name='lucidworks.connector.sampling.Req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='configId', full_name='lucidworks.connector.sampling.Req.configId', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='limit', full_name='lucidworks.connector.sampling.Req.limit', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='waitSec', full_name='lucidworks.connector.sampling.Req.waitSec', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='refresh', full_name='lucidworks.connector.sampling.Req.refresh', index=3,
      number=4, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='maxFieldLength', full_name='lucidworks.connector.sampling.Req.maxFieldLength', index=4,
      number=5, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='excludeBinaryFields', full_name='lucidworks.connector.sampling.Req.excludeBinaryFields', index=5,
      number=6, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=101,
  serialized_end=226,
)


_DEBUGREQ = _descriptor.Descriptor(
  name='DebugReq',
  full_name='lucidworks.connector.sampling.DebugReq',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='sampleReq', full_name='lucidworks.connector.sampling.DebugReq.sampleReq', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='pipelineId', full_name='lucidworks.connector.sampling.DebugReq.pipelineId', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='parserId', full_name='lucidworks.connector.sampling.DebugReq.parserId', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=228,
  serialized_end=331,
)


_RES = _descriptor.Descriptor(
  name='Res',
  full_name='lucidworks.connector.sampling.Res',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='pipelineDocuments', full_name='lucidworks.connector.sampling.Res.pipelineDocuments', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=333,
  serialized_end=394,
)


_DEBUGRES = _descriptor.Descriptor(
  name='DebugRes',
  full_name='lucidworks.connector.sampling.DebugRes',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='stageConfigs', full_name='lucidworks.connector.sampling.DebugRes.stageConfigs', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='output', full_name='lucidworks.connector.sampling.DebugRes.output', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='outputEncounteredCount', full_name='lucidworks.connector.sampling.DebugRes.outputEncounteredCount', index=2,
      number=3, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=397,
  serialized_end=535,
)

_DEBUGREQ.fields_by_name['sampleReq'].message_type = _REQ
_RES.fields_by_name['pipelineDocuments'].message_type = lucidworks_dot_property__map__pb2._MAP
_DEBUGRES.fields_by_name['stageConfigs'].message_type = lucidworks_dot_property__map__pb2._MAP
_DEBUGRES.fields_by_name['output'].message_type = lucidworks_dot_property__map__pb2._MAP
DESCRIPTOR.message_types_by_name['Req'] = _REQ
DESCRIPTOR.message_types_by_name['DebugReq'] = _DEBUGREQ
DESCRIPTOR.message_types_by_name['Res'] = _RES
DESCRIPTOR.message_types_by_name['DebugRes'] = _DEBUGRES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Req = _reflection.GeneratedProtocolMessageType('Req', (_message.Message,), dict(
  DESCRIPTOR = _REQ,
  __module__ = 'lucidworks.connector.sampling_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.connector.sampling.Req)
  ))
_sym_db.RegisterMessage(Req)

DebugReq = _reflection.GeneratedProtocolMessageType('DebugReq', (_message.Message,), dict(
  DESCRIPTOR = _DEBUGREQ,
  __module__ = 'lucidworks.connector.sampling_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.connector.sampling.DebugReq)
  ))
_sym_db.RegisterMessage(DebugReq)

Res = _reflection.GeneratedProtocolMessageType('Res', (_message.Message,), dict(
  DESCRIPTOR = _RES,
  __module__ = 'lucidworks.connector.sampling_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.connector.sampling.Res)
  ))
_sym_db.RegisterMessage(Res)

DebugRes = _reflection.GeneratedProtocolMessageType('DebugRes', (_message.Message,), dict(
  DESCRIPTOR = _DEBUGRES,
  __module__ = 'lucidworks.connector.sampling_pb2'
  # @@protoc_insertion_point(class_scope:lucidworks.connector.sampling.DebugRes)
  ))
_sym_db.RegisterMessage(DebugRes)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n.com.lucidworks.fusion.proto.connector.samplingB\016SamplingProtosP\000'))
# @@protoc_insertion_point(module_scope)
