schema = {
    "type": "object",
    "name": "lucidworks.python",
    "title": "SDK - Python Sample",
    "description": "Connector for Python",
    "required": ["id", "properties", "pipelineId"],
    "properties": {
        "id": {
            "type": "string",
            "title": "Configuration ID",
            "description": "A unique identifier for this Configuration.",
            "order": -100
        },
        "pipelineId": {
            "type": "string",
            "title": "Pipeline ID",
            "description": "Name of the IndexPipeline used for processing output.",
            "order": -90,
            "minLength": 1
        },
        "parserId": {
            "type": "string",
            "title": "Parser ID",
            "description": "The Parser to use in the associated IndexPipeline.",
            "order": -80
        },
        "description": {
            "type": "string",
            "title": "Description",
            "description": "Optional description",
            "hints": ["lengthy"],
            "order": -70,
            "maxLength": 125
        },
        "type": {
            "type": "string",
            "title": "Type",
            "description": "A type ID for this connector.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "collectionId": {
            "type": "string",
            "title": "Collection ID",
            "description": "The associated content Collection.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "loggingLevel": {
            "type": "string",
            "title": "Logging Level",
            "description": "The logging level used during a Connector job.",
            "enum": ["INFO", "DEBUG", "OFF", "FATAL", "ERROR", "WARN", "TRACE", "ALL", "system-default"],
            "default": "system-default",
            "hints": ["advanced"],
            "order": 0
        },
        "created": {
            "type": "string",
            "title": "Date Created",
            "description": "The date at which this Configuration was created.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "modified": {
            "type": "string",
            "title": "Date Modified",
            "description": "The date at which this Configuration was last modified.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "properties": {
            "type": "object",
            "title": "Python Sample Plugin Properties",
            "description": "Plugin specific properties.",
            "required": ["initialItemIds"],
            "properties": {
                "initialItemIds": {
                    "type": "array",
                    "title": "Initial Item IDs",
                    "description": "The initial file or folder IDs to crawl.",
                    "order": 0,
                    "items": {
                        "type": "string",
                        "order": 0
                    }
                }
            },
            "order": 0,
            "propertyGroups": []
        }
    },
    "category": "Sample Connectors",
    "order": 0,
    "propertyGroups": []
}

invalid_schema = {
    "type": "object",
    "name": "connector.csharp",
    "title": "CSharp Sample Connector",
    "description": "A CSharp Connector",
    "required": ["id", "properties", "pipelineId"],
    "properties": {
        "properties": {
            "type": "object",
            "title": "Fetcher Properties",
            "required": ["initialFilePaths"],
            "properties": {
                "processors": {
                    "type": "object",
                    "title": "Processor Configuration",
                    "description": "Processor Configuration",
                    "required": [],
                    "properties": {
                        "contentProperties": {
                            "type": "object",
                            "title": "Content Processor",
                            "required": [],
                            "properties": {
                                "name": {
                                    "type": "string",
                                    "default": "content-processor",
                                    "order": 0
                                }
                            },
                            "order": 0,
                            "propertyGroups": []
                        }
                    },
                    "order": 0,
                    "propertyGroups": []
                },
                "initialFilePaths": {
                    "type": "array",
                    "order": 0,
                    "items": {
                        "type": "string",
                        "order": 0
                    }
                }
            },
            "order": 0,
            "propertyGroups": []
        },
        "id": {
            "type": "string",
            "title": "Configuration ID",
            "description": "A unique identifier for this Configuration.",
            "order": -100
        },
        "pipelineId": {
            "type": "string",
            "title": "Pipeline ID",
            "description": "Name of the IndexPipeline used for processing output.",
            "order": -99,
            "minLength": 1
        },
        "parserId": {
            "type": "string",
            "title": "Parser ID",
            "description": "The Parser to use when processing output in the associated IndexPipeline.",
            "order": -97
        },
        "type": {
            "type": "string",
            "title": "Type",
            "description": "A type ID for this connector.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "created": {
            "type": "string",
            "title": "Date Created",
            "description": "The date at which this Configuration was created.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "collectionId": {
            "type": "string",
            "title": "Collection ID",
            "description": "The associated content Collection.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "loggingLevel": {
            "type": "string",
            "title": "Logging Level",
            "description": "A dynamic logging level used during a Connector job.",
            "default": "system-default",
            "allowedValues": ["TRACE", "system-default", "ALL", "ERROR", "INFO", "DEBUG", "FATAL", "OFF", "WARN"],
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "modified": {
            "type": "string",
            "title": "Date Modified",
            "description": "The date at which this Configuration was last modified.",
            "hints": ["readonly", "hidden"],
            "order": 0
        },
        "maxThreads": {
            "type": "integer",
            "title": "Max Threads",
            "description": "The maximum number of threads used to for fetch processing.",
            "default": 4,
            "allowedValues": [],
            "hints": ["advanced"],
            "order": 0
        },
        "maxNodes": {
            "type": "integer",
            "title": "Max Nodes",
            "description": "The maximum number of nodes called on during a Job.",
            "default": 1,
            "allowedValues": [],
            "hints": ["advanced"],
            "order": 0
        },
        "clearDbOnStart": {
            "type": "boolean",
            "title": "Reset database on start",
            "description": "When checked, this option forces the database to be cleared before running the job.",
            "default": False,
            "allowedValues": [],
            "hints": ["advanced"],
            "order": 0
        }
    },
    "category": "SDK CSharp",
    "order": 0,
    "propertyGroups": []
}
