import Queue
import json
import threading
import uuid
import logging
import grpc
import config
import sys

from google.rpc import status_pb2
from lucidworks.connector.common_pb2 import ConnectorConfig
from lucidworks.connector.connector_plugin_pb2 import PluginCall, ConnectReq, PluginInfoRes, PluginValidateConfigRes

ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))

logger = logging.getLogger('python-grpc')
logger.addHandler(ch)
logger.setLevel(logging.DEBUG)


class MessagesReceiver(threading.Thread):
    def __init__(self, lock, response_iterator, queue):
        threading.Thread.__init__(self)
        self.lock = lock
        self.queue = queue
        self.response_iterator = response_iterator

    def run(self):
        try:
            for message in self.response_iterator:
                if "connectRes" in str(message):
                    logger.info("connectRes")
                elif "validateConfigReq" in str(message):
                    logger.info("validate")
                    self.lock.acquire()
                    connector_config = ConnectorConfig()

                    res = PluginValidateConfigRes()
                    res.connectorConfig.MergeFrom(connector_config)

                    plugin_call = PluginCall()
                    plugin_call.requestId = message.requestId
                    plugin_call.validateConfigRes.MergeFrom(res)

                    self.queue.put(plugin_call)
                    self.lock.notifyAll()
                    self.lock.release()
                    logger.info("finished validate")
                else:
                    logger.info("no recognized message")
        except grpc._channel._Rendezvous as err:
            print(err)


class Connect(object):

    def __init__(self, stub):
        self.stub = stub

    def build_plugin(self, requestId):
        info = PluginInfoRes()
        info.id = requestId
        info.type = "connector.csharp"
        info.schema = json.dumps(config.invalid_schema)
        info.pluginVersion = "0.0.0.1"
        info.fusionVersion = "4.0.1-SNAPSHOT"

        req = ConnectReq()
        req.info.MergeFrom(info)

        plugin_call = PluginCall()
        plugin_call.status.MergeFrom(status_pb2.Status())
        plugin_call.requestId = requestId
        plugin_call.connectReq.MergeFrom(req)

        return plugin_call

    def send_messages(self, queue_send_messages, lock):
        while True:
            lock.acquire()

            logger.info("wait for message")

            yield queue_send_messages.get()

            logger.info("send message")

            lock.wait()

        logger.info("finished")

    def deploy_plugin(self):
        queue = Queue.Queue()

        request_id = str(uuid.uuid4())

        queue.put(self.build_plugin(request_id))

        lock = threading.Condition()

        response_iterator = self.stub.Connect(self.send_messages(queue, lock))

        receiver = MessagesReceiver(lock, response_iterator, queue)

        receiver.start()
        receiver.join()

    def execute(self):
        self.deploy_plugin()